<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public $timestamps = false;

    /**
     * Scope a query for search by First and Last names.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSearchByNames($query, $search)
    {
        return $query->where('FirstName', 'like', '%' . $search . '%')
            ->orWhere('LastName', 'like', '%' . $search . '%');
    }

    /**
     * @param $search
     * @return mixed
     */
    public function search($search)
    {
        return $this->searchByNames($search)->take(10)->get()->toArray();
    }
}
