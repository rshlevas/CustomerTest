<?php

namespace App\Helpers\Searcher\Interfaces;


interface SearcherInterface
{
    public function run(string $search);

}