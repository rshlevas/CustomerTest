<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.10.17
 * Time: 9:27
 */

namespace App\Helpers\Searcher\Interfaces;


interface RequestTransformerInterface
{
    public function prepare(string $search);
}