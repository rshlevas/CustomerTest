<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.10.17
 * Time: 10:32
 */

namespace App\Helpers\Searcher\Interfaces;


interface SorterInterface
{
    public function prepare($data);
}