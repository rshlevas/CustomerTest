<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.10.17
 * Time: 9:15
 */

namespace App\Helpers\Searcher;



use App\Helpers\Searcher\Interfaces\RequestTransformerInterface;
use App\Helpers\Searcher\Interfaces\SearcherInterface;
use Illuminate\Database\Eloquent\Model;

abstract class Searcher implements SearcherInterface
{
    /**
     * @var
     */
    protected $transformer;

    /**
     * @var
     */
    protected $objectRepo;

    /**
     * CustomerSearcher constructor.
     * @retun void
     */
    public function __construct(RequestTransformerInterface $requestTransformer, Model $models)
    {
        $this->transformer = $requestTransformer;
        $this->objectRepo = $models;
    }

    /**
     * Starts search process and returns obtained data
     *
     * @param string $search
     * @return mixed
     */
    public function run(string $search)
    {
        $request = $this->prepareSearchRequest($search);
        $data = $this->getSearchData($request);
        return $data;
    }

    /**
     * Prepares search request for proper view
     *
     * @param $data
     * @return mixed
     */
    protected abstract function prepareSearchRequest($search);

    /**
     * Returns data by prepared search request
     *
     * @param $request
     * @return mixed
     */
    protected abstract function getSearchData($request);
}