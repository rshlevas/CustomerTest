<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.10.17
 * Time: 9:49
 */

namespace App\Helpers\Searcher;

use App\Customer;
use App\Helpers\Searcher\Helpers\CustomerSearcherSorter;
use App\Helpers\Searcher\Helpers\RequestTransformer;
use Illuminate\Support\Facades\Cache;

class CustomerSearcher extends Searcher
{
    /**
     * @var CustomerSearcherSorter
     */
    protected $sorter;

    /**
     * CustomerSearcher constructor.
     * @param RequestTransformer $requestTransformer
     * @param Customer $customers
     */
    public function __construct(
        RequestTransformer $requestTransformer,
        Customer $customers,
        CustomerSearcherSorter $sorter
    )
    {
        parent::__construct($requestTransformer, $customers);
        $this->sorter = $sorter;
    }

    /**
     * Prepares search request for proper view
     *
     * @param $data
     * @return mixed
     */
    protected function prepareSearchRequest($search)
    {
        return $this->transformer->prepare($search);
    }

    /**
     * Returns data by prepared search request
     *
     * @param $request
     * @return mixed
     */
    protected function getSearchData($request)
    {
        $data = [];
        foreach ($request as $keyWord) {
            $response = $this->getFromCache($keyWord);
            if (!$response) {
                $response = $this->getFromRepo($keyWord);
                $this->putToCache($keyWord, $response, env('CACHE_STORAGE_TIME'));
            }
            $data[] = $response;
        }
        return $this->sortSearchData($data);
    }

    /**
     * Made a search by keyword in object repository
     *
     * @param $key
     * @return mixed
     */
    protected function getFromRepo($key)
    {
        return $this->objectRepo->search($key);
    }

    /**
     * Made a search by keyword in cache
     *
     * @param $key
     * @return mixed
     */
    protected function getFromCache($key)
    {
        return Cache::tags('search')->get($key);
    }

    /**
     * Puts data into cache for some minutes
     *
     * @param $key
     * @param $value
     * @param $minutes
     * @return mixed
     */
    protected function putToCache($key, $value, $minutes)
    {
        return Cache::tags('search')->put($key, $value, $minutes);
    }

    /**
     * Sort obtained data and makes it unique
     *
     * @param $data
     * @return array
     */
    protected function sortSearchData($data)
    {
        return $this->sorter->prepare($data);
    }
}