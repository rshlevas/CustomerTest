<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.10.17
 * Time: 10:34
 */

namespace App\Helpers\Searcher\Helpers;


use App\Helpers\Searcher\Interfaces\SorterInterface;

class CustomerSearcherSorter implements SorterInterface
{
    /**
     * Sortes data, makes it unique and return array
     *
     * @param $data
     * @return array
     */
    public function prepare($data)
    {
        $sortedData = $this->getUniqueObjectsArray($data);
        return $this->sortByRelevance($sortedData);
    }

    /**
     * @param $data
     * @return array
     */
    protected function getUniqueObjectsArray($data) {
        $result = [];
        foreach ($data as $collection) {
            if(!empty($collection)) {
                foreach ($collection as $objectData) {
                    if (array_key_exists($objectData['id'], $result)) {
                        $result[$objectData['id']]['count']++;
                    } else {
                        $result[$objectData['id']] = $objectData;
                        $result[$objectData['id']]['count'] = 1;
                    }
                }
            }
        }
        return $result;
    }


    protected function sortByRelevance($array)
    {
        uasort($array, function($a, $b) {
            if ($a['count'] == $b['count']) {
                return 0;
            }
            return ($a['count'] > $b['count']) ? -1 : 1;
        });
        return $array;
    }
}