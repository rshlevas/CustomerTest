<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.10.17
 * Time: 9:23
 */

namespace App\Helpers\Searcher\Helpers;


use App\Helpers\Searcher\Interfaces\RequestTransformerInterface;

class RequestTransformer implements RequestTransformerInterface
{
    /**
     * Function that transform search phrase and returns
     * array with unique key words
     *
     * @param $search
     * @return array
     */
    public function prepare(string $search)
    {
        $data = explode(' ', strtolower(trim($search)));
        $dataUnique = array_values(array_unique($data));
        return array_diff($dataUnique, ['']);
    }
}