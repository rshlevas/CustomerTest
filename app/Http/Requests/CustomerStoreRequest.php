<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'FirstName' => 'required|regex:/^[a-zA-Z-]+$/u',
            'LastName' => 'required|regex:/^[a-zA-Z-]+$/u',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'FirstName.regex' => 'Only letters and hyphen are allowed',
            'LastName.regex'  => 'Only letters and hyphen are allowed',
        ];
    }
}
