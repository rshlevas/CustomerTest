<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 27.10.17
 * Time: 10:03
 */

namespace App\Http\Controllers;

use App\Http\Requests\CustomerStoreRequest;
use App\Customer;
use Illuminate\Support\Facades\Cache;

class CustomerController extends Controller
{
    public function create()
    {
        return view('customers/create');
    }

    public function edit($id)
    {
        $customer = Customer::find($id);
        if (!$customer) {
           return redirect('/');
        }
        return view('customers/edit', ['customer' => $customer]);
    }

    public function store(CustomerStoreRequest $request)
    {
        $data = $request->all();
        $customer = null;
        if (isset($data['id'])) {
            $customer = Customer::find($data['id']);
        } else {
            $customer = new Customer();
        }
        $customer->FirstName = $data['FirstName'];
        $customer->LastName = $data['LastName'];
        if ($customer->save()) {
            Cache::tags('search')->flush();
        }

        return redirect('/');
    }

    public function delete($id)
    {
        $customer = Customer::find($id);
        if ($customer) {
            $customer->delete();
            Cache::tags('search')->flush();
        }
        return redirect('/');
    }


}