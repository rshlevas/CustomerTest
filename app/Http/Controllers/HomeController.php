<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.10.17
 * Time: 14:55
 */

namespace App\Http\Controllers;

use App\Customer;
use App\Helpers\Searcher\CustomerSearcher;
use App\Http\Requests\CustomerSearchRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
       return view('homepage');
    }

    /**
     * @param Request $request
     * @param CustomerSearcher $searcher
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(CustomerSearchRequest $request, CustomerSearcher $searcher)
    {
        $customers = $searcher->run($request->input('search'));
        $html = view('includes/customer_table', ['customers' => $customers])->render();
        return response()->json(['message' => 'success', 'html' => $html]);
    }

    public function tryDo()
    {

    }
}

