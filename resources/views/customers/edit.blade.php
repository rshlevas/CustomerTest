@extends('layouts.app')

@section('title', 'Edit Customer')

@section('content')

    <div class="c-content clearfix">
        <div class="main-block">
            <div class="form-container">
                <form class="c-form" name="customers-create" method="POST"
                      action="{{ route('customer_save') }}">
                    {{ csrf_field() }}
                    <div class="c-form__title">Editing of customer</div>
                    @if ($errors->any())
                        <div class="c-form__errors">
                            @foreach ($errors->all() as $error)
                                <div class="error__message">{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif

                    <div class="c-form__group">
                        <label class="c-form__label" for="FirstName">First Name:</label>
                        <input type="text" class="c-form__text-input"
                               id="FirstName" name="FirstName" required autofocus minlength="2"
                               placeholder="Customer first name"
                               value="{{ $customer->FirstName }}"
                        >
                    </div>
                    <div class="c-form__group">
                        <label class="c-form__label" for="LastName">Last Name:</label>
                        <input type="text" class="c-form__text-input"
                               id="LastName" name="LastName" required autofocus minlength="2"
                               placeholder="Customer last name"
                               value="{{ $customer->LastName }}"
                        >
                    </div>
                    <button type="submit" class="c-form__btn">Edit</button>
                    <input type="hidden" name="id" value="{{ $customer->id }}">
                </form>
            </div>
        </div>
    </div>

@endsection