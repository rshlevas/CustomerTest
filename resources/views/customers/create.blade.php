@extends('layouts.app')

@section('title', 'Create Customer')

@section('content')
    <div class="c-content clearfix">
        <div class="main-block">
            <div class="form-container">
                <form class="c-form" name="customers-create" method="POST"
                      action="{{ route('customer_save') }}">
                    {{ csrf_field() }}
                    <div class="c-form__title">Creating of customer</div>
                    @if ($errors->any())
                        <div class="c-form__errors">
                            @foreach ($errors->all() as $error)
                                <div class="error__message">{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif

                    <div class="c-form__group">
                        <label class="c-form__label" for="FirstName">First Name:</label>
                        <input type="text" class="c-form__text-input"
                               id="FirstName" name="FirstName" required autofocus minlength="2"
                               placeholder="Customer first name"
                        >
                    </div>
                    <div class="c-form__group">
                        <label class="c-form__label" for="LastName">Last Name:</label>
                        <input type="text" class="c-form__text-input"
                               id="LastName" name="LastName" required autofocus minlength="2"
                               placeholder="Customer last name"
                        >
                    </div>
                    <button type="submit" class="c-form__btn">Create</button>
                </form>
            </div>
        </div>
    </div>
@endsection