<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') </title>

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<div id="app">
    <div class="c-header">
        <div class="header__nav">
           <ul class="header__nav-bar --inline">
             <li class="nav-bar__list"><a class="nav-bar__links" href="{{ route('home') }}">Home</a></li>
             <li class="nav-bar__list"><a class="nav-bar__links" href="#">About</a></li>
           </ul>
        </div>
        <div class="header__search-form">
            <form class="c-form header__search-form" name="search" method="POST" action="{{ route('search_customer') }}">
                {{ csrf_field() }}
                <input type="text" class="c-form__text-input"
                               id="search" name="search" required autofocus minlength="2"
                               placeholder="Search"
                >
                <button type="submit" class="c-form__btn">GO</button>
            </form>
        </div>
    </div>



    @yield('content')
</div>
<script src="{{ asset('js/main.js') }}"></script>

</body>
</html>
