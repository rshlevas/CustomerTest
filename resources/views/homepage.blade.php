@extends('layouts.app')

@section('title', 'Homepage')

@section('content')
    <div class="c-content clearfix">
        <div class="main-block">
            <div class="home-content">
                <div class="home-content__title">Find your own customer!!!</div>
                <div class="home-content__item">
                    <img class="item__image" src="{{ asset('images/item_1.jpg') }}">
                    <div class="item__content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In posuere
                        volutpat mi, vestibulum dictum massa pharetra at. Nulla semper pulvinar auctor. Quisque nec
                        dolor felis. Nulla id tellus venenatis, fermentum dolor nec, congue nibh. Vivamus a ante sit
                        amet eros laoreet mollis a a sem. Nulla vel dui nulla. In hac habitasse platea dictumst. Sed
                        a mattis nulla. Integer rutrum lorem sit amet diam malesuada, pharetra euismod lacus
                        fringilla. Donec scelerisque neque at viverra dictum. Sed volutpat risus tellus. Aliquam mi
                        mauris, pretium id metus a, tincidunt viverra velit. Morbi consectetur aliquam massa
                        volutpat rutrum. Ut id ex orci. Nunc ut velit nisl.
                        <a class="c-link-button --right" href="#">READ MORE >></a>
                    </div>
                </div>
                <div class="home-content__item">
                    <img class="item__image" src="{{ asset('images/item_2.jpg') }}">
                    <div class="item__content">Nulla sit amet turpis nec mi faucibus gravida. Curabitur bibendum arcu
                        leo, volutpat congue nunc fermentum a. Etiam tincidunt aliquet consequat. Nunc tempor, purus
                        sit amet feugiat vulputate, urna purus laoreet tellus, non aliquet erat erat eu mi. Proin a
                        ullamcorper lorem, sed faucibus enim. Vivamus nec nisl eu lacus bibendum lobortis ac nec
                        metus. Nam tempor, felis consequat molestie accumsan, nulla est bibendum diam, nec porta
                        metus lacus ac felis. Vestibulum aliquam volutpat velit, et pharetra eros. Aliquam auctor
                        justo nec lacus fermentum fringilla maximus venenatis enim. Suspendisse potenti. Donec at
                        metus purus. Sed efficitur gravida libero, id tincidunt nulla vehicula sit amet. Quisque
                        aliquam ac magna sit amet euismod. Vivamus sed magna egestas, tincidunt felis et,
                        consectetur mi.
                        <a class="c-link-button --right" href="#">READ MORE >></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection