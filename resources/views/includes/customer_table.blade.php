
@if (empty($customers))
    <div class="main-block__message">Unfortunately, nothing was found!</div>
@else
    <div class="--shifted-right-down"><a class="c-link-button" href="{{ route('customer_create') }}">Add Customer</a></div>
    <table class="table table-hover">
        <caption>Search list</caption>
        <thead>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Second Name</th>
                <th class="table__crud-header">Managing</th>
            </tr>
        </thead>
        <tbody>
        @foreach($customers as $customer)
            <tr>
                <td>{{ $customer['id'] }}</td>
                <td>{{ $customer['FirstName'] }}</td>
                <td>{{ $customer['LastName'] }}</td>
                <td class="table__crud-buttons">
                    <a class="c-link-button" href="{{ route('customer_edit', ['id' => $customer['id']]) }}">Edit</a>
                    <a class="c-link-button" href="{{ route('customer_delete', ['id' => $customer['id']]) }}">Delete</a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif