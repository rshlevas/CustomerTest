/**
 * Customers search request
 */

$('form[name = "search"]').submit(function (e) {
    e.preventDefault();
    var form = $(this);
    var query = form.serialize();
    var url = "/search";
    $.ajax({
        url: url,
        type: 'POST',
        data: query,
        mimeType: "multipart/form-data",
        dataType: 'json',
        success: function (data) {
            $(".main-block").html(data.html);
        }
    });
    return false;
});