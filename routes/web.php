<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::post('/search', 'HomeController@search')->name('search_customer');

// Customer

Route::match(['get', 'post'], '/customer/edit/{id}', 'CustomerController@edit')->name('customer_edit');

Route::match(['get', 'post'], '/customer/delete/{id}', 'CustomerController@delete')->name('customer_delete');

Route::post('/customer/save', 'CustomerController@store')->name('customer_save');

Route::get('/customer/create', 'CustomerController@create')->name('customer_create');

Route::get('/try', 'HomeController@tryDo');



