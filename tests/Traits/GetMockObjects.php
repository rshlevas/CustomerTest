<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.10.17
 * Time: 16:11
 */

namespace Tests\Traits;

use App\Helpers\Searcher\Helpers\CustomerSearcherSorter;
use App\Helpers\Searcher\Helpers\RequestTransformer;
use App\Customer;


trait GetMockObjects
{
    protected function getMockCustomerWithoutData()
    {
        $customer = $this->getMockBuilder(Customer::class)->getMock();
        return $customer;
    }

    protected function getMockSorter()
    {
        return $this->getMockBuilder(CustomerSearcherSorter::class)
            ->setMethods(null)->getMock();
    }

    protected function getMockTransformer()
    {
        return $this->getMockBuilder(RequestTransformer::class)
            ->setMethods(null)->getMock();
    }

    protected function getMockCustomerWithData($data, $count)
    {
        $customer = $this->getMockBuilder(Customer::class)->setMethods(['search'])->getMock();
        if ($count === 1) {
            $customer->expects($this->once())
                ->method('search')
                ->with($data['key'])
                ->will($this->returnValue($data['value']));
        } elseif ($count === 2) {
            $customer->expects($this->exactly(2))
                ->method('search')
                ->withConsecutive([$this->equalTo($data[0]['key'])],
                    [$this->equalTo($data[1]['key'])])
                ->willReturnOnConsecutiveCalls($data[0]['value'], $data[1]['value']);
        }
        return $customer;
    }
}