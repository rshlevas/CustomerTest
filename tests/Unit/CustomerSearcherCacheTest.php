<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.10.17
 * Time: 15:27
 */

namespace Tests\Unit;

use App\Helpers\Searcher\CustomerSearcher;
use Tests\TestCase;
use Tests\Traits\GetMockObjects;
use Illuminate\Support\Facades\Cache;

class CustomerSearcherCacheTest extends TestCase
{
    use GetMockObjects;

    private $searcher;

    public function setUp()
    {
        parent::setUp();
        $this->searcher = new CustomerSearcher(
            $this->getMockTransformer(),
            $this->getMockCustomerWithoutData(),
            $this->getMockSorter()
        );
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->searcher = null;
    }

    /**
     *
     * @dataProvider cacheDataProvider
     * @group searcher
     */
    public function testRunFromCache($cache, $string, $result)
    {
        $this->setToCacheForTime($cache['key'], $cache['value']);
        $searchResult = $this->searcher->run($string);
        $this->assertEquals($result, $searchResult);
    }


    public function cacheDataProvider()
    {
        $value = [

                0 => [
                    'id' => 1
                ],
                1 => [
                    'id' => 5
                ],
                2 => [
                    'id' => 1
                ],
                3 => [
                    'id' => 2
                ],
                4 => [
                    'id' => 5
                ],
                5 => [
                    'id' => 5
                ]
        ];

        $cache = [
            'value' => $value,
            'key' => 'joe'
        ];

        $string = 'JOE    joe  JOe';

        $result = [
            5 => [
                'id' => 5,
                'count' => 3,
            ],
            1 => [
                'id' => 1,
                'count' => 2,
            ],
            2 => [
                'id' => 2,
                'count' => 1,
            ],
        ];

        return [
            [$cache, $string, $result]
        ];
    }



    protected function setToCacheForTime($key, $value, $minutes = 1)
    {
        return Cache::put($key, $value, $minutes);
    }
}