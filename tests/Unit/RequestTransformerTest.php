<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.10.17
 * Time: 14:44
 */

namespace Tests\Unit;

use App\Helpers\Searcher\Helpers\RequestTransformer;
use Tests\TestCase;

class RequestTransformerTest extends TestCase
{
    /**
     * @var
     */
    private $transformer;

    /**
     * Init function
     */
    public function setUp()
    {
        parent::setUp();
        $this->transformer = new RequestTransformer();
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->transformer = null;
    }

    /**
     *
     * @dataProvider prepareDataProvider
     * @group reqTransform
     */
    public function testPrepare($string, $result)
    {
        $array = $this->transformer->prepare($string);
        $this->assertEquals($result, $array);
    }

    /**
     *
     *
     */
    public function prepareDataProvider()
    {
        return [
            [
                'string' => 'david DAVID DaVid',
                'result' => [
                    0 => 'david',
                ],
            ],
            [
                'string' => 'john JOHN asds JOHNSON jim',
                'result' => [
                    0 => 'john',
                    1 => 'asds',
                    2 => 'johnson',
                    3 => 'jim'
                ],
            ],
        ];
    }
}