<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.10.17
 * Time: 16:41
 */

namespace Tests\Unit;

use App\Helpers\Searcher\CustomerSearcher;
use Tests\TestCase;
use Tests\Traits\GetMockObjects;

class CustomerSearcherDBTest extends TestCase
{
    use GetMockObjects;

    private $searcher;

    private $data;

    private $searchString = 'Jason jim      JASON   JIm';


    public function setUp()
    {
        parent::setUp();
        $this->data = $this->getData();
        $this->searcher = new CustomerSearcher(
            $this->getMockTransformer(),
            $this->getMockCustomerWithData($this->data, 2),
            $this->getMockSorter()
        );
    }

    public function tearDown()
    {
        parent::tearDown();
        $this->searcher = null;
    }

    /**
     *
     * @dataProvider resultDataProvider
     * @group searcher
     */
    public function testRun($result)
    {
        $searchResult = $this->searcher->run($this->searchString);
        $this->assertEquals($result, $searchResult);
    }


    public function resultDataProvider()
    {
        $result = [
            5 => [
                'id' => 5,
                'count' => 4,
            ],
            1 => [
                'id' => 1,
                'count' => 2,
            ],
            2 => [
                'id' => 2,
                'count' => 1,
            ],
            3 => [
                'id' => 3,
                'count' => 1,
            ],
            4 => [
                'id' => 4,
                'count' => 1,
            ],
        ];

        return [
            [$result]
        ];
    }


    protected function getData()
    {
        $key1 = 'jason';

        $key2 = 'jim';

        $value1 = [

            0 => [
                'id' => 1
            ],
            1 => [
                'id' => 5
            ],
            2 => [
                'id' => 1
            ],
            3 => [
                'id' => 2
            ],
            4 => [
                'id' => 5
            ],
            5 => [
                'id' => 5
            ]
        ];

        $value2 = [
            0 => [
                'id' => 3
            ],
            1 => [
                'id' => 4
            ],
            2 => [
                'id' => 5
            ],
        ];


        return [
            0 => [
                'value' => $value1,
                'key' => $key1,
            ],
            1 => [
                'value' => $value2,
                'key' => $key2,
            ]
        ];
    }

}