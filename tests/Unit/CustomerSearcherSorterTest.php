<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 26.10.17
 * Time: 15:02
 */

namespace Tests\Unit;

use App\Helpers\Searcher\Helpers\CustomerSearcherSorter;
use Tests\TestCase;

class CustomerSearcherSorterTest extends TestCase
{
    /**
     * @var
     */
    private $sorter;

    /**
     * initial function
     */
    public function setUp()
    {
        parent::setUp();
        $this->sorter = new CustomerSearcherSorter();
    }

    /**
     *  Destructor
     */
    public function tearDown()
    {
        parent::tearDown();
        $this->sorter = null;
    }

    /**
     *
     *
     * @dataProvider sorterDataProvider
     * @group sorter
     */
    public function testPrepare($array, $result)
    {
       $sortedArray = $this->sorter->prepare($array);
       $this->assertEquals($sortedArray, $result);
    }

    public function sorterDataProvider()
    {
        $test_1 = [
            0 => [
                0 => [
                    'id' => 2
                ],
                1 => [
                    'id' => 1
                ]
            ],

            1 => [
                0 => [
                    'id' => 3
                ],
                1 => [
                    'id' => 2
                ]
            ]
        ];

        $test_2 = [
            0 => [],

            1 => [
                0 => [
                    'id' => 1
                ],
                1 => [
                    'id' => 5
                ],
                2 => [
                    'id' => 1
                ],
                3 => [
                    'id' => 5
                ],
                4 => [
                    'id' => 5
                ],
                5 => [
                    'id' => 5
                ]
            ],
            2 => [],
        ];

        $result_1 = [
            2 => [
                'id' => 2,
                'count' => 2,
            ],
            1 => [
                'id' => 1,
                'count' => 1,
            ],
            3 => [
                'id' => 3,
                'count' => 1,
            ]
        ];

        $result_2 = [
            5 => [
                'id' => 5,
                'count' => 4,
            ],
            1 => [
                'id' => 1,
                'count' => 2,
            ],
        ];
        return [
            [$test_1, $result_1],
            [$test_2, $result_2],
        ];
    }
}